package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class pasajeroPage {
    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(xpath = "//input[@id='txt_nombre[suite][cab1][1]']") private WebElement txt_nombre;
    @FindBy(xpath = "//input[@id='txt_apellido[suite][cab1][1]']") private WebElement txt_apellido;
    @FindBy(xpath = "//input[@id='txt_fecha_nacimiento[suite][cab1][1]']") private WebElement txt_nacimiento;
    @FindBy(xpath = "//div[@id='ui-datepicker-div']/table/tbody/tr[1]/td[7]/a") private WebElement fecha_dia_nacimiento;
    @FindBy(xpath = "//select[@id='sel_nacion[suite][cab1][1]']//option") private List<WebElement> lst_nacion;
    @FindBy(xpath = "//select[@id='sel_tpdoc[suite][cab1][1]']//option") private List<WebElement> lst_tipo_documento;
    @FindBy(xpath = "//input[@id='txt_nroid[suite][cab1][1]']") private WebElement txt_nro_documento;
    @FindBy(xpath = "//select[@id='sel_sexo[suite][cab1][1]']//option") private List<WebElement> lst_sexo;
    @FindBy(xpath = "//input[@id='txt_telefono[suite][cab1][1]']") private WebElement txt_telefono;
    @FindBy(xpath = "//input[@id='txt_mail[suite][cab1][1]']") private WebElement txt_mail;
    @FindBy(xpath = "//input[@id='txt_mail_conf[suite][cab1][1]']") private WebElement txt_conf_mail;
    @FindBy(xpath = "//button[@id='btnContinuarPas']") private WebElement btn_continuar;

    public pasajeroPage(WebDriver dvr) {

        driver=dvr;
        wait = new WebDriverWait(driver,30);
        PageFactory.initElements(driver, this);

    }

    public void ingresarNombreyApellidos(String nombre, String apellido) {
        txt_nombre.sendKeys(nombre);
        txt_apellido.sendKeys(apellido);
        txt_nacimiento.click();
        fecha_dia_nacimiento.click();
    }

    public void ingresarDocumentosIdentidad( int nroDoc) {
        seleccionaNacionalidad();
        seleccionaTipoDocumento();
        txt_nro_documento.sendKeys(String.valueOf(nroDoc));

    }

    private void seleccionaNacionalidad(){
        for (WebElement element : lst_nacion) {
            if (element.getText().trim().equals("Peru")) {
                element.click();
                break;
            }
        }
    }

    private void seleccionaTipoDocumento(){
        for (WebElement element : lst_tipo_documento) {
            if (element.getText().trim().equals("Identification Card")) {
                element.click();
                break;
            }
        }

    }

    public void ingresarTelefonoyCorreo(int telefono, String correo) {
        seleccionaSexo();
        txt_telefono.sendKeys(String.valueOf(telefono));
        txt_mail.sendKeys(correo);
        txt_conf_mail.sendKeys(correo);
        btn_continuar.click();
    }

    private void seleccionaSexo(){
        for (WebElement element : lst_sexo) {
            if (element.getText().trim().equals("Male")) {
                element.click();
                break;
            }
        }

    }
}
