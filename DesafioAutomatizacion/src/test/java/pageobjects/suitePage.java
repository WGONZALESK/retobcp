package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class suitePage {

    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(xpath = "//select[@name='selectRooms[suite]']//option") private List<WebElement> lst_suite_option;
    @FindBy(xpath = "//input[@id='continuar_bae']")private  WebElement btn_continue;

    public suitePage(WebDriver dvr) {

        driver=dvr;
        wait = new WebDriverWait(driver,30);
        PageFactory.initElements(driver, this);

    }

    public void seleccionoSuiteCabins(int numCabina) {

        for (WebElement element : lst_suite_option) {
            System.out.println(element.getText());
            if (element.getText().trim().equals(numCabina+" CABIN")) {
                element.click();
                break;
            }
        }

        btn_continue.click();
    }


}

