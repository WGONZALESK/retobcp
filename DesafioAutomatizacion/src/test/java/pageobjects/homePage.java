package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class homePage {
    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(xpath = "//div[@class='input-radio']//label[@for='oneway']") private WebElement rdb_oneway;
    @FindBy(xpath= "//select[@id='destinoSelect']//option") private List<WebElement> lst_destino;
    @FindBy(xpath= "//select[@id='rutaSelect']//option") private List<WebElement> lst_ruta;
    @FindBy(xpath= "//select[@id='cbTrenSelect']//option") private List<WebElement> lst_servicio;
    @FindBy(xpath = "//input[@id='salida']") private WebElement txt_salida;
    @FindBy(xpath = "//a[@title='Next']") private WebElement btn_fecha_siguiente;
    @FindBy(xpath = "//*[@id='ui-datepicker-div']/table/tbody/tr[5]/td[4]/a") private WebElement btn_dia_disponible;
    @FindBy(xpath = "//button[@id='btn_search']") private WebElement btn_submit;

    public homePage(WebDriver dvr) {

        driver=dvr;
        wait = new WebDriverWait(driver,30);
        PageFactory.initElements(driver, this);
    }

    public void seleccionarIda(){
       wait.until(ExpectedConditions.visibilityOf(rdb_oneway));
       rdb_oneway.click();
    }

    public void seleccionarDestino(String destino)  {

        for (WebElement element : lst_destino) {
            if (element.getText().trim().equals(destino)) {
                element.click();
                break;
            }
        }

    }

    public void seleccionarRuta(String ruta){
        for (WebElement element : lst_ruta) {
            if (element.getText().trim().equals(ruta)) {
                element.click();
                break;
            }
        }
    }

    public void seleccionarServicio(String servicio){
        for (WebElement element : lst_servicio) {
            if (element.getText().trim().equals(servicio)) {
                element.click();
                break;
            }
        }
    }

    public void seleccionarSalida() {
        txt_salida.click();
        btn_fecha_siguiente.click();
        btn_fecha_siguiente.click();
        btn_fecha_siguiente.click();
        btn_fecha_siguiente.click();
        btn_dia_disponible.click();
    }

    public void buscarTicketsdeTren() {
        btn_submit.click();
    }
}
