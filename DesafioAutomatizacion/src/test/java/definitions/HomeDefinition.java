package definitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageobjects.homePage;
import pageobjects.suitePage;

public class HomeDefinition {
    homePage home;
    suitePage suite;

   public HomeDefinition(){
       home = new homePage(Hooks.driver);
       suite = new suitePage(Hooks.driver);
   }

    @Given("ingreso a home")
    public void ingresoAHome() {
      Hooks.driver.get("https://www.perurail.com/");

    }

    @When("ingreso los parametros de busqueda")
    public void ingresoLosParametrosDeBusqueda() {
        home.seleccionarIda();
    }


    @And("selecciono el destino {string}")
    public void seleccionoLosParametrosDeBusquedaCusco(String destino) throws InterruptedException {
         home.seleccionarDestino(destino);

    }


    @And("selecciono la ruta {string}")
    public void seleccionoLaRutaPuno(String ruta) {
       home.seleccionarRuta(ruta);

    }

    @And("selecciono el servicio {string}")
    public void seleccionoElServicio(String servicio) {
       home.seleccionarServicio(servicio);
    }

    @And("selecciono la fecha de salida para buscar tickets")
    public void seleccionoLaFechaDeSalida() {
       home.seleccionarSalida();
       home.buscarTicketsdeTren();
    }





}
