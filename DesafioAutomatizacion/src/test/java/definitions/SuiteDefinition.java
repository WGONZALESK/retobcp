package definitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import pageobjects.suitePage;

public class SuiteDefinition {
    suitePage suite;

    public SuiteDefinition() {
        suite = new suitePage(Hooks.driver);

    }

    @Then("selecciono {int} SUITE CABINS")
    public void seleccionoSUITECABINS(int numCabina) {
        suite.seleccionoSuiteCabins(numCabina);

    }


}
