package definitions;

import io.cucumber.java.en.And;
import pageobjects.pasajeroPage;
import pageobjects.suitePage;

public class PasajeroDefinition {
    pasajeroPage pasajero;

    public PasajeroDefinition() {
        pasajero = new pasajeroPage(Hooks.driver);
    }

    @And("ingreso datos del pasajero {string} y {string}")
    public void ingresoDatosDelPasajeroWilmerYGonzales(String nombre, String apellido) {
        pasajero.ingresarNombreyApellidos(nombre,apellido);

    }

    @And("ingreso documento con nro {int}")
    public void ingresoDocumentoDNIConNro( int nroDoc) {
        pasajero.ingresarDocumentosIdentidad(nroDoc);
    }

    @And("telefono {int} y correo {string}")
    public void telefonoYCorreoWgonzaleskGmailCom(int telefono, String correo) {
        pasajero.ingresarTelefonoyCorreo(telefono,correo);
    }
}
