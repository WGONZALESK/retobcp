Feature: Historia busqueda Ticket de Tren
  Scenario: Destino CUSCO con servicio BELMOND ANDEAN EXPLORER
    Given ingreso a home
    When ingreso los parametros de busqueda
    And selecciono el destino 'CUSCO'
    And selecciono la ruta 'PUNO > CUSCO'
    And selecciono el servicio 'ANDEAN EXPLORER, A BELMOND TRAIN'
    And selecciono la fecha de salida para buscar tickets
    Then selecciono 1 SUITE CABINS
    And ingreso datos del pasajero 'Wilmer' y 'Gonzales'
    And ingreso documento con nro 44113607
    And telefono 944994253 y correo 'wgonzalesk@gmail.com'



