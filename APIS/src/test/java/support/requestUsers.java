package support;

import io.restassured.response.Response;

public class requestUsers {
    apiHelper api;
    public  static Response userResponse;

    public requestUsers(){
        api= new apiHelper();
    }

    public void getUsers(){
        userResponse = api.sendGet("https://gorest.co.in/public/v2/users");
    }

    public void getUser(String id){
        userResponse = api.sendGet("https://gorest.co.in/public/v2/users/"+id);
    }
}
