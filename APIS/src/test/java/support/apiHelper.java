package support;

import io.restassured.response.Response;
import static io.restassured.RestAssured.given;

public class apiHelper {

    public Response sendGet(String url){
    Response response = given().baseUri(url).get();
    return response;
    }
}
