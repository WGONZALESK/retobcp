package definitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import org.junit.Assert;
import support.requestUsers;

public class UsuarioDefinition {
    requestUsers requestUsers;

    public UsuarioDefinition(){
        requestUsers = new requestUsers();
    }

    @Given("realizo peticion del servicio Users")
    public void realizoPeticionDelServicioUsers() {
        requestUsers.getUsers();

    }

    @When("valido Endpoint users se encuentre disponible")
    public void validoEndpointUsersSeEncuentreDisponible() {
        Response response = requestUsers.userResponse;
        Assert.assertEquals(200,response.getStatusCode());
        System.out.println("El EndPoint Users obtuvo con codigo de respuesta :"+response.getStatusCode());
    }

    @Then("verifico que el Usuario con el ID {string} está activo")
    public void verificoQueElUsuarioConElIDEstáActivo(String id) {
        requestUsers.getUser(id);
        Response response = requestUsers.userResponse;
        ResponseBody body = response.getBody();
       // System.out.println(body.asString());
        JsonPath json=new JsonPath(body.asString());
        Assert.assertEquals("active",json.getString("status"));
        System.out.println("el id del usuario tiene el estado: "+json.getString("status"));
    }
}
